//  @ Project :SIFAGE Untitled
//  @ Date : 20/03/2013 20/03/2013
//  @ Author : Marcelo do Nascimento Rocha

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SIFAGE.Daos;
using SIFAGE.Models;
using NHibernate;
using NHibernate.Cfg;


namespace SIFAGE.Daos
{
    public class MensagemDao
    {

        private static MensagemDao instance;
        private static ISessionFactory MensagemDaoSession { get; set; }

        /*O metodo construtor da classe � mantido como privado por
         * estarmos utilizando o padr�o de projeto singleton        
         */
        private MensagemDao()
        {
            var configuration = new Configuration();
            configuration.Configure()
            .AddAssembly(typeof(Mensagem).Assembly);
            MensagemDaoSession = configuration.BuildSessionFactory();

        }


        /*O metodo construtor da classe � mantido como privado por
         * estarmos utilizando o padr�o de projeto singleton        
         * este metodo � um metodo static para chamar uma instancia unica da classe
         */
        public static MensagemDao getInstance()
        {
            if (instance == null)
            {
                instance = new MensagemDao();
                return instance;
            }
            else{
                MensagemDao aux = instance;
                return aux;
            }
        }

        public void Add(Mensagem menssagem)
        {
            using (ISession session = MensagemDaoSession.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Save(menssagem);
                transaction.Commit();
                session.Close();
                session.Dispose();
            }
        }

        public void Update(Mensagem menssagem)
        {
            using (ISession session = MensagemDaoSession.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Update(menssagem);
                transaction.Commit();
                session.Close();
                session.Dispose();
            }
        }

        public void Remove(Mensagem menssagem)
        {
            using (ISession session = MensagemDaoSession.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Delete(menssagem);
                transaction.Commit();
                session.Close();
                session.Dispose();

            }
        }

        public Mensagem GetById(int Id)
        {
            using (ISession session = MensagemDaoSession.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Mensagem menssagem = session.Get<Mensagem>(Id);
                transaction.Commit();
                session.Close();
                session.Dispose();
                return menssagem;
            }
        }

    
        public List<Mensagem> ListAll()
        {
            try
            {
                List<Mensagem> Lista = new List<Mensagem>();
                using (ISession session = MensagemDaoSession.OpenSession())
                {
                    IQuery query = session.CreateQuery("select m from Mensagem m where m.Estatus = false");
                    query.List(Lista);

                }
                return Lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public List<Mensagem> ListarTodasPermitidas()
        {
            try
            {
                List<Mensagem> Lista = new List<Mensagem>();
                using (ISession session = MensagemDaoSession.OpenSession())
                {
                    IQuery query = session.CreateQuery("select p from Mensagem p");
                    query.List(Lista);

                }
                return Lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

   }
}