-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Abr 17, 2013 as 07:47 PM
-- Versão do Servidor: 5.5.8
-- Versão do PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `sifage`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administradores`
--

CREATE TABLE IF NOT EXISTS `administradores` (
  `idadministradores` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idadministradores`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `administradores`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE IF NOT EXISTS `funcionario` (
  `idfuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `idmensagem` varchar(10) DEFAULT NULL,
  `nome` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`idfuncionario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`idfuncionario`, `idmensagem`, `nome`) VALUES
(1, NULL, 'BRASILCENTER\\MARLO/'),
(2, NULL, 'BRASILCENTER\\MARLO/'),
(3, NULL, 'BRASILCENTER\\MARLO/'),
(4, NULL, 'BRASILCENTER\\MARLO/'),
(5, NULL, 'BRASILCENTER\\MARLO/'),
(6, NULL, 'BRASILCENTER\\MARLO/'),
(7, NULL, 'BRASILCENTER\\MARLO/'),
(8, NULL, 'BRASILCENTER\\MARLO/'),
(9, NULL, 'BRASILCENTER\\MARLO/'),
(10, NULL, 'BRASILCENTER\\MARLO/'),
(11, NULL, 'BRASILCENTER\\MARLO/'),
(12, NULL, 'BRASILCENTER\\MARLO/'),
(13, NULL, 'BRASILCENTER\\MARLO/');

-- --------------------------------------------------------

--
-- Estrutura da tabela `menssagem`
--

CREATE TABLE IF NOT EXISTS `menssagem` (
  `idmenssagem` int(11) NOT NULL AUTO_INCREMENT,
  `estatus` tinyint(1) NOT NULL,
  `conteudo` text,
  `host_name` varchar(45) DEFAULT NULL,
  `funcionario_idfuncionario` int(11) NOT NULL,
  `endereco_ip` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idmenssagem`),
  KEY `fk_menssagem_funcionario1` (`funcionario_idfuncionario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `menssagem`
--

INSERT INTO `menssagem` (`idmenssagem`, `estatus`, `conteudo`, `host_name`, `funcionario_idfuncionario`, `endereco_ip`) VALUES
(5, 0, 'Estou digitando mais uma outra mensagem de te', NULL, 5, NULL),
(6, 0, 'To enable communications with the application', NULL, 6, NULL),
(12, 0, '\r\nCargo	Cidade	Mínimo(CLT)	Médio(CLT)	Máximo(CLT)	Pesquisa de Vagas\r\nAdministração Banco de Dados - DBA	RIO DE JANEIRO - RJ		 		Vagas Administração Banco de Dados - DBA\r\nAdministrador de Redes	RIO DE JANEIRO - RJ		 		Vagas Administrador de Redes\r\nAdministrador de Sistemas	RIO DE JANEIRO - RJ		 		Vagas Administrador de Sistemas\r\nAdministrador Linux	RIO DE JANEIRO - RJ		 		Vagas Administrador Linux\r\nAnalista de Desenvolvimento	RIO DE JANEIRO - RJ		 		Vagas Analista de Desenvolvimento\r\nAnalista de Implantação	RIO DE JANEIRO - RJ		 		Vagas Analista de Implantação\r\nAnalista de Negocios	RIO DE JANEIRO - RJ		 		Vagas Analista de Negocios\r\nAnalista de Processos	RIO DE JANEIRO - RJ		 		Vagas Analista de Processos\r\nAnalista de Produção	RIO DE JANEIRO - RJ		 		Vagas Analista de Produção\r\nAnalista de Projetos	RIO DE JANEIRO - RJ		 		Vagas Analista de Projetos\r\nAnalista de Qualidade	RIO DE JANEIRO - RJ		 		Vagas Analista de Qualidade\r\nAnalista de Redes	RIO DE JANEIRO - RJ		 		Vagas Analista de Redes\r\nAnalista de Requisitos	RIO DE JANEIRO - RJ		 		Vagas Analista de Requisitos\r\nAnalista de Sistemas	RIO DE JANEIRO - RJ		 		Vagas Analista de Sistemas\r\nAnalista de Suporte	RIO DE JANEIRO - RJ		 		Vagas Analista de Suporte\r\nAnalista de Testes	RIO DE JANEIRO - RJ		 		Vagas Analista de Testes\r\nAnalista Funcional	RIO DE JANEIRO - RJ		 		Vagas Analista Funcional\r\nAnalista Microinformatica	RIO DE JANEIRO - RJ		 		Vagas Analista Microinformatica\r\nAnalista Programador	RIO DE JANEIRO - RJ		 		Vagas Analista Programador\r\nAnalista Programador Oracle - DBA	RIO DE JANEIRO - RJ		 		Vagas Analista Programador Oracle - DBA\r\nArquiteto	RIO DE JANEIRO - RJ		 		Vagas Arquiteto\r\nArquiteto de Sistemas	RIO DE JANEIRO - RJ		 		Vagas Arquiteto de Sistemas\r\nArquiteto de Software	RIO DE JANEIRO - RJ		 		Vagas Arquiteto de Software\r\nAssistente de Service Desk	RIO DE JANEIRO - RJ		 		Vagas Assistente de Service Desk\r\nAtendente de Help Desk	RIO DE JANEIRO - RJ		 		Vagas Atendente de Help Desk\r\nComercial	RIO DE JANEIRO - RJ		 		Vagas Comercial\r\nConsultor	RIO DE JANEIRO - RJ		 		Vagas Consultor\r\nConsultor de Vendas - TI	RIO DE JANEIRO - RJ		 		Vagas Consultor de Vendas - TI\r\nConsultor SAP	RIO DE JANEIRO - RJ		 		Vagas Consultor SAP\r\nCoordenador	RIO DE JANEIRO - RJ		 		Vagas Coordenador\r\nCoordenador de projetos	RIO DE JANEIRO - RJ		 		Vagas Coordenador de projetos\r\nCoordenador de TI	RIO DE JANEIRO - RJ		 		Vagas Coordenador de TI\r\nDesenvolvedor	RIO DE JANEIRO - RJ		 		Vagas Desenvolvedor\r\nDesenvolvedor de Software	RIO DE JANEIRO - RJ		 		Vagas Desenvolvedor de Software\r\nDesenvolvedor/Programador .Net	RIO DE JANEIRO - RJ		 		Vagas Desenvolvedor/Programador .Net\r\nDesenvolvedor/Programador Java	RIO DE JANEIRO - RJ		 		Vagas Desenvolvedor/Programador Java\r\nDesenvolvedor/Programador PHP	RIO DE JANEIRO - RJ		 		Vagas Desenvolvedor/Programador PHP\r\nDesenvolvedor/Programador Web	RIO DE JANEIRO - RJ		 		Vagas Desenvolvedor/Programador Web\r\nDesenvolvimento/Programação	RIO DE JANEIRO - RJ		 		Vagas Desenvolvimento/Programação\r\nDesigner Gráfico	RIO DE JANEIRO - RJ		 		Vagas Designer Gráfico\r\nEngenheiro de Software	RIO DE JANEIRO - RJ		 		Vagas Engenheiro de Software\r\nERP/MRP	RIO DE JANEIRO - RJ		 		Vagas ERP/MRP\r\nEspecialista	RIO DE JANEIRO - RJ		 		Vagas Especialista\r\nEstagiário	RIO DE JANEIRO - RJ		 		Vagas Estagiário\r\nGerente de Contas TI	RIO DE JANEIRO - RJ		 		Vagas Gerente de Contas TI\r\nGerente de Projetos	RIO DE JANEIRO - RJ		 		Vagas Gerente de Projetos\r\nGerente de TI	RIO DE JANEIRO - RJ		 		Vagas Gerente de TI\r\nGestor de Tecnologia da Informação	RIO DE JANEIRO - RJ		 		Vagas Gestor de Tecnologia da Informação\r\nLíder Técnico	RIO DE JANEIRO - RJ		 		Vagas Líder Técnico\r\nOperador	RIO DE JANEIRO - RJ		 		Vagas Operador\r\nPesquisas/Projetos	RIO DE JANEIRO - RJ		 		Vagas Pesquisas/Projetos\r\nProgramador	RIO DE JANEIRO - RJ		 		Vagas Programador\r\nProgramador C#	RIO DE JANEIRO - RJ		 		Vagas Programador C#\r\nProgramador Delphi	RIO DE JANEIRO - RJ		 		Vagas Programador Delphi\r\nProgramador Visual Basic	RIO DE JANEIRO - RJ		 		Vagas Programador Visual Basic\r\nSupervisor	RIO DE JANEIRO - RJ		 		Vagas Supervisor\r\nTecnico Microinformatica	RIO DE JANEIRO - RJ		 		Vagas Tecnico Microinformatica\r\nTécnico	RIO DE JANEIRO - RJ		 		Vagas Técnico\r\nTécnico de Suporte	RIO DE JANEIRO - RJ		 		Vagas Técnico de Suporte\r\nTécnico em Telecomunicações	RIO DE JANEIRO - RJ		 		Vagas Técnico em Telecomunicações\r\nTrainee	RIO DE JANEIRO - RJ		 		Vagas Trainee\r\nWebdesigner	RIO DE JANEIRO - RJ		 		Vagas Webdesigner', NULL, 12, NULL),
(13, 0, 'ahsdjhaskljdhkasjdhkajsdhkjashdkjash  mensagem de teste para o Paiva', NULL, 13, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `respondedor`
--

CREATE TABLE IF NOT EXISTS `respondedor` (
  `idrespondedor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL,
  `senha` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`idrespondedor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `respondedor`
--

INSERT INTO `respondedor` (`idrespondedor`, `nome`, `matricula`, `senha`) VALUES
(1, 'Suporte', '007007', 'embr@tel21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta`
--

CREATE TABLE IF NOT EXISTS `resposta` (
  `idresposta` int(11) NOT NULL AUTO_INCREMENT,
  `conteudo` varchar(45) DEFAULT NULL,
  `respondedor_idrespondedor` int(11) NOT NULL,
  `menssagem_idmenssagem` int(11) NOT NULL,
  PRIMARY KEY (`idresposta`),
  KEY `fk_resposta_respondedor` (`respondedor_idrespondedor`),
  KEY `fk_resposta_menssagem1` (`menssagem_idmenssagem`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `resposta`
--


--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `menssagem`
--
ALTER TABLE `menssagem`
  ADD CONSTRAINT `fk_menssagem_funcionario1` FOREIGN KEY (`funcionario_idfuncionario`) REFERENCES `funcionario` (`idfuncionario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `resposta`
--
ALTER TABLE `resposta`
  ADD CONSTRAINT `fk_resposta_menssagem1` FOREIGN KEY (`menssagem_idmenssagem`) REFERENCES `menssagem` (`idmenssagem`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_resposta_respondedor` FOREIGN KEY (`respondedor_idrespondedor`) REFERENCES `respondedor` (`idrespondedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;
