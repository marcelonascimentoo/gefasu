﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SIFAGE.Models;
using SIFAGE.Daos;
namespace SIFAGE.Controllers
{
    public class RespondedorController : Controller
    {
        //
        // GET: /Respondedor/

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult LogIn(Respondedor respondedor)
        {
            Respondedor resp = RespondedorDao.getInstance().GetByAccount(respondedor.Nome, respondedor.Senha);
            if (resp != null)
            {
                RespondedorDao dao = RespondedorDao.getInstance();

                Session["usuario"] = respondedor.Nome;
                Session["userName"] = "Wellcome " + respondedor.Nome + "!!";
                return RedirectToAction("ListarMensagens", "Mensagem");
            }
            else
            {
                return RedirectToAction("LoginInvalido", "Respondedor");
            
            }
        }

        public ActionResult LoginInvalido()
        {



            return View();
        }

        public ActionResult LoginValido()
        {



            return View();
        }


    }
}
