﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SIFAGE.Daos;
using SIFAGE.Models;
using SIFAGE.Filters;
using System.Net;

namespace SIFAGE.Controllers
{
    public class MensagemController : Controller
    {        
        public ActionResult Index()
        {
           
           return View();
        }

        [HttpPost]
        public ActionResult GravarMensagem(Mensagem mensagem)
        {   try
            {
                MensagemDao msgDao = MensagemDao.getInstance();
               // FuncionarioDao funcDao = FuncionarioDao.getInstance();
              // funcDao.Add(mensagem.Funcionario);            
                msgDao.Add(mensagem);

                return RedirectToAction("Index", "Mensagem");
            }
            catch(Exception ex )
            {
                throw ex;
                
            }        
        }

        /*Este metódo libera a visualização de todas as menssagens para a gerente geral         
         */ 
        [AuthenticationFilter]
        public ActionResult ListarMensagens()
        {
            List<Mensagem> mensagens  = MensagemDao.getInstance().ListAll();
            List<Mensagem> mensagens2 = new List<Mensagem>();

           foreach(var mensagensTratadas in mensagens){
               
               if (mensagensTratadas.Conteudo.Length >= 40)
               {
                   mensagensTratadas.Conteudo = mensagensTratadas.Conteudo.Substring(0, 40);
                   mensagens2.Add(mensagensTratadas);
               }
               
            }
        
           return View(mensagens2);
        }


        // Este método deverá ser validado para exigir login sempre que solicitado
        public ActionResult MostrarMensagemParaGerente()
        {
            MensagemDao msgDao = MensagemDao.getInstance();
            List<Mensagem> mensagens = msgDao.ListAll();
            return View(mensagens);
        }




        // Este método deverá ser validado para exigir login sempre que solicitado
        /* Este é para o Gerente responder a Mensagem
         */
        [HttpGet]
        [AuthenticationFilter]
        public ActionResult ResponderMensagemEnviadaAoGerente(int Id)
        { 
            MensagemDao msgDao = MensagemDao.getInstance();
            Mensagem mensagem = msgDao.GetById(Id);
            ViewBag.mensagem = mensagem;

            return View();
        }
        // Este método deverá ser validado para exigir login sempre que solicitado
        /* Este é para o Gerente responder a Mensagem
         */
        [HttpPost]
        public ActionResult ProcessarMensagemRespondidaPeloGerente(Resposta resposta)
        {
            RespostaDao respDao = RespostaDao.getInstance();
            Mensagem mensagem = MensagemDao.getInstance().GetById(resposta.Mensagem.Id);
            resposta.Mensagem = mensagem;
            resposta.Mensagem.Estatus = true;
            //mensagem.Resposta.Add(resposta);            
            Respondedor respondedor = RespondedorDao.getInstance().GetById(resposta.Respondedor.Id);
            resposta.Respondedor = respondedor;            
            respDao.Add(resposta);
            //MensagemDao.getInstance().Update(mensagem);


            return RedirectToAction("Index","Home");
        }

        // Este método deverá ser validado para exigir login sempre que solicitado
        /* Este é para o Gerente responder a Mensagem
         */
        public ActionResult IgnorarMensagemPeloGerente(double mensagemId)
        {

            return View();
        }
        
        // Este método deverá ser validado para exigir login sempre que solicitado
        /* Este é para o Gerente responder a Mensagem
         */
        public ActionResult MostrarMuralDeMensagens()
        {
            List<Resposta> respostas = RespostaDao.getInstance().ListAll();


            return View(respostas);

        }



    }
}
